//@ts-nocheck
import {Client} from "pg";

const ConnectPG = new Client({
            port: 0,
            host: "",
            user: "",
            password: "",
            database: ""
    })


export const queryPG = async (query: string) =>{
    //e.g = 'SELECT * FROM "(table name)" WHERE (column) = $1',[(value -> $1)]
    //e.g = 'UPDATE "(table name)" SET "(column update)" = $1 WHERE "(column condition)" = $2', [(value -> $1), (value -> $2)]
    //e.g = 'INSERT INTO "(table name)" ("(column)") VALUES ($(value)) RETURNING *',[(value)]
    //e.g = 'DELETE FROM "(table name)" WHERE "(column)" = $1',[value -> $1]
    return await ConnectPG.query(query)
}