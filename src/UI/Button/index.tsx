//@ts-nocheck
import React from 'react';
import {Button, IconButton} from '@material-ui/core';

interface IButtonProps {
    children?: any,
    classes?: any,
    style?: any,
    icon?: any,
    startIcon?: any,
    color?: any, //primary, inherit, secondary
    endIcon?: any,
    href?: String,
    disabled?: boolean,
    onPress?: any,
    onClick?: any,
}

export default (props: IButtonProps) => {
    if(props.onPress){
        props.onClick = props.onPress;
    }
    
    if(props.icon){
        return <IconButton {...props} style={{margin: 30}} color={props.color} onClick={props.onClick} children={props.icon} />
    }
    
    return <Button {...props} style={{margin: 30}} color={props.color} onClick={props.onClick} children={props.children} />
}