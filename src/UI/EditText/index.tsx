import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import {formatNumberUSD, formatNumberIDR} from './../../Utils/Money'

import * as React from 'react';

export default (props: any) => {
    let variant: string;
    let value: any;
    let inputValue: any;

    if(props.lock){
        variant = "filled";
    }else{
        variant = "outlined"
    }

    return <TextField {...props} style={{margin: 30}}   variant={variant} disabled={props.lock} 
                InputProps={{
                    startAdornment: <InputAdornment position='start'>{props.prefix}</InputAdornment>,
                    endAdornment: <InputAdornment position='end'>{props.suffix}</InputAdornment>,
                }}/>;
}
