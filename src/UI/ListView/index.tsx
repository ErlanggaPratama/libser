import * as React from 'react';

export default (props: any) => {
    let direction: string = 'column';
    if(props.orientation === 'vertical'){
        direction = 'column';
    }else if(props.orientation === 'horizontal'){
        direction = 'row';
    }
    return <div {...props} style={{
        display: 'flex',
        flexDirection: direction,
        ...props.style
    }} />;
}
