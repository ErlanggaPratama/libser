//@ts-nocheck
//import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '50%',
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }),
);