//@ts-nocheck
import React from 'react';
import './App.css';
import {Button, ListView, TextView, EditText} from './UI';
import {useStyles} from './UI/style'
import {formatUSD, formatRupiah, getDiscount, formatNumberIDR, formatNumberUSD} from './Utils/Money'
import { setCountByDay, setCountByHour } from './Utils/Date'

function App() {
  const [values, setValues] = React.useState({
    numberformat: 0 as number,
  });

  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
    console.log(event.target.value);
  };

  return (
    <ListView style={useStyles.root} >
      <Button size="medium" variant="outlined" color="inherit">
        {setCountByDay(new Date(), 2, true)}
      </Button>
      <ListView style={{padding: 20}}></ListView>
      <Button variant="contained" color="primary">
        {getDiscount(100000, 50)}
      </Button>
      <ListView style={{padding: 20}}></ListView>
      <EditText prefix="Rp. " lock={false} label="Cost" name="numberformat" value={formatRupiah(values.numberformat)} onChange={handleChange}></EditText>
      <ListView style={{padding: 20}}></ListView>
      <TextView>{formatRupiah(26000, "Rp. ")}</TextView>
    </ListView>
  );
}

export default App;