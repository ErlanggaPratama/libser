import axios from 'axios';
import { Base64 } from 'js-base64';

export const createInvoice = async (data:any, token: any) =>{
    let tokenGenerate = Base64.encode(token);
    await axios({
            method: "POST",
            url: "https://api.xendit.co/v2/invoices",
            headers: {
            "cache-control": "no-status",
            "content-type": "application/json",
            authorization: "Basic " + tokenGenerate
            },
            data: data
        }).then(async response => {
          return response.data;
        }).catch(err => {
          return err.response.data;
        });
};

export const getInvoice = async (invoiceId:any, token:any) =>{
        let tokenGenerate = Base64.encode(token);
        await axios({
                method: 'GET',
                url: 'https://api.xendit.co/v2/invoices/'+invoiceId,
                headers:{ 
                    'cache-contorl': 'no-cache',
                    authorization: 'Basic ' + tokenGenerate,
                }
            }).then(result =>{
                return result.data;
            }).catch(error=>{
                return error.result;
            })
}

export const expiredInvoice = async (invoiceId:any, token:any) =>{
    let tokenGenerate = Base64.encode(token)
    await axios({
            method: "POST",
            url: `https://api.xendit.co/invoices/${invoiceId}/expire!`,
            headers: {
            "cache-control": "no-status",
            "content-type": "application/json",
            authorization: "Basic " + tokenGenerate
            },
            data: {
                invoiceID: invoiceId
            }
        }).then(async response => {
          return response.data;
        }).catch(err => {
          return err.response.data;
        });
}